import threading
import random
import time

# Definir variables atómicas
tickets_disponibles = 50
lock_tickets = threading.Lock()

compradorSimultaneo = threading.Semaphore(5)        #cantidad de compras simultaneas

tickets_vendidos = {}                               #diccionario de ticket vendidos con comprador
cola_compradores = []                               #compradores en cola de compra
barrera_compradores = threading.Barrier(5)          #sincronización de compradores

class Comprador(threading.Thread):
    def __init__(self, nombre):
        threading.Thread.__init__(self)
        self.nombre = nombre
    
    def run(self):
        global tickets_disponibles
        global tickets_vendidos
        
        # Coloca al comprador en un tiempo de espera
        tiempo_espera = random.randint(1, 5)
        print(f'{self.nombre} está haciendo cola, esperando {tiempo_espera} segundos...')
        time.sleep(tiempo_espera)
        
        # Añadirse a la cola de compradores en espera
        cola_compradores.append(self)
        
        # Esperar a que todos los compradores estén listos
        barrera_compradores.wait()
        
        # Esperar a que haya un espacio disponible para comprar un ticket
        compradorSimultaneo.acquire()
        
        # Intentar comprar un ticket
        with lock_tickets:
            if tickets_disponibles > 0:
                tickets_disponibles -= 1
                if self.nombre not in tickets_vendidos:
                    tickets_vendidos[self.nombre] = 1
                else:
                    tickets_vendidos[self.nombre] += 1
                print(f'{self.nombre} ha comprado un ticket')
            else:
                print(f'{self.nombre} no ha podido comprar un ticket')
        
        # Liberar el semáforo de los compradores
        compradorSimultaneo.release()
        
#Lista de compradores
compradores = []
for i in range(60):
    compradores.append(Comprador(f'Comprador {i+1}'))

# Iniciar a los compradores
for comprador in compradores:
    comprador.start()

for comprador in compradores:
    comprador.join()

# Imprimir el registro de los tickets vendidos
print(f'Tickets vendidos: {tickets_vendidos}')
print(f'Tickets disponibles: {tickets_disponibles}')
