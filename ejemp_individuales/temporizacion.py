import sched, time, threading

s = sched.scheduler(time.time, time.sleep)

def tarea():
    print("Se está ejecutando la tarea.")

def planificar_tarea():
    print("Planificando la tarea para dentro de 5 segundos.")
    s.enter(5, 1, tarea, ())
    s.run()

hilo = threading.Thread(target=planificar_tarea)
hilo.start()
hilo.join()

print("La tarea ha sido ejecutada")