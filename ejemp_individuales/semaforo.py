import threading

# Creamos un semáforo con un valor de 1
semaphore = threading.Semaphore(1)

# Función que imprimirá los números en orden ascendente
def imprimir_numeros(numero_hilo):
    # Adquirimos el semáforo
    semaphore.acquire()
    # Imprimimos el número del hilo
    print("Número del hilo", numero_hilo)
    # Liberamos el semáforo
    semaphore.release()

# Creamos tres hilos
hilo1 = threading.Thread(target=imprimir_numeros, args=(1,))
hilo2 = threading.Thread(target=imprimir_numeros, args=(2,))
hilo3 = threading.Thread(target=imprimir_numeros, args=(3,))

# Iniciamos los hilos
hilo1.start()
hilo2.start()
hilo3.start()

# Esperamos a que terminen los hilos
hilo1.join()
hilo2.join()
hilo3.join()