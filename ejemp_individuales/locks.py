import threading
thread = threading.Thread

class CuentaBancaria:
    def __init__(self, saldo):
        self.saldo = saldo
        self.lock = threading.Lock()

    def deposito(self, valorDepositar):
        with self.lock:
            self.saldo += valorDepositar
            print("Depositó: $", valorDepositar , ", su nuevo saldo es de: $", self.saldo)

    def retiro(self, valorRetiro):
        with self.lock:
            self.saldo -= valorRetiro
            print("Retiró: $", valorRetiro , ", su saldo actual: $", self.saldo)

    def consultarSaldo(self):
        with self.lock:
            print("Su saldo actual de la cuenta es de: $", self.saldo)

    def transferir(self, valor, cuentaDestino):
        with self.lock:
            print('Inicio de Transferencia')
            self.saldo -= valor
            cuentaDestino.deposito(valor)
            print("Ha transferido $", valor, ", su saldo actual es: $", self.saldo, "\n")


# Creamos las cuentas bancarias
cuentaPedro = CuentaBancaria(650)
cuentaJuan = CuentaBancaria(480)

#realizamos las transacciones
transfPedroToJuan = thread(target=cuentaPedro.transferir, args=(100, cuentaJuan))
retiroPedro = thread(target=cuentaPedro.retiro, args=(50,))
depositoJuan = thread(target=cuentaJuan.deposito, args=(70,))
juanConsultaSaldo = thread(target=cuentaJuan.consultarSaldo)

transfPedroToJuan.start()
retiroPedro.start()
depositoJuan.start()
juanConsultaSaldo.start()

# asegura de ejecutarse todos los hilos
transfPedroToJuan.join()
retiroPedro.join()
depositoJuan.join()
juanConsultaSaldo.join()

print("Transacciones realizadas")

