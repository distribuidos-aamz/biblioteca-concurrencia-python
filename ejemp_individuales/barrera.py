import threading

class MiHilo(threading.Thread):
    def __init__(self, nombre, barrera):
        threading.Thread.__init__(self)
        self.nombre = nombre
        self.barrera = barrera

    def run(self):
        print("{} alcanzó la barrera".format(self.nombre))
        self.barrera.wait()  # El hilo espera a que todos los demás hilos alcancen la barrera
        print("{} continuó ejecutándose".format(self.nombre))

# Creamos una barrera que espera a que 3 hilos lleguen a ella
barrera = threading.Barrier(3)

# Creamos 3 hilos y los iniciamos
hilo1 = MiHilo("Hilo 1", barrera)
hilo2 = MiHilo("Hilo 2", barrera)
hilo3 = MiHilo("Hilo 3", barrera)

hilo1.start()
hilo2.start()
hilo3.start()

# Esperamos a que los hilos terminen su ejecución
hilo1.join()
hilo2.join()
hilo3.join()
