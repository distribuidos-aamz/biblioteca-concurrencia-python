import threading
import time
import concurrent.futures

counter = 0
counter_lock = threading.Lock()

def increment():
    global counter
    with counter_lock:
        counter += 1

def worker():
    for i in range(100):
        increment()
        print(counter)

if __name__ == "__main__":
    # Se crean 10 hilos que incrementan el contador
    with concurrent.futures.ThreadPoolExecutor(max_workers=10) as executor:
        for i in range(10):
            executor.submit(worker)

    # Se espera a que los hilos terminen y se imprime el resultado final
    print("Contador: ", counter)
