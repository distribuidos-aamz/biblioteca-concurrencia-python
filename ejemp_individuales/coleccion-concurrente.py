import threading
import queue

class ProcesadorMensajes(threading.Thread):
    def __init__(self, cola):
        threading.Thread.__init__(self)
        self.cola = cola

    def run(self):
        while True:
            mensaje = self.cola.get()
            print("Procesando mensaje: ", mensaje)
            self.cola.task_done()

# Creamos una cola
cola_mensajes = queue.Queue()

# Creamos los procesadores
procesador_1 = ProcesadorMensajes(cola_mensajes)
procesador_2 = ProcesadorMensajes(cola_mensajes)

# Iniciamos los procesadores
procesador_1.start()
procesador_2.start()

# Agregamos algunos mensajes a la cola
for i in range(10):
    cola_mensajes.put("Mensaje {}".format(i))

# Esperamos hasta que todos los mensajes hayan sido procesados
cola_mensajes.join()
procesador_1.join()
procesador_2.join()

print("Todos los mensajes han sido procesados")
