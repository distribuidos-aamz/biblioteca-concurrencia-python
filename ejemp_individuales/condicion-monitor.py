import threading

class Buffer:
    def __init__(self):
        self.buffer = []
        self.monitor = threading.Condition()

    def producir(self, item):
        with self.monitor:
            self.buffer.append(item)
            self.monitor.notify()

    def consumir(self):
        with self.monitor:
            while not self.buffer:
                self.monitor.wait()
            return self.buffer.pop(0)

def productor(buffer):
    for i in range(10):
        buffer.producir(i)

def consumidor(buffer):
    while True:
        item = buffer.consumir()
        print("Consumiendo item", item)

buffer = Buffer()
threading.Thread(target=productor, args=(buffer,)).start()
threading.Thread(target=consumidor, args=(buffer,)).start()
